package br.com.a5solutions.rest.connections;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import br.com.a5solutions.beans.TokenConnectionBean;
import br.com.a5solutions.utils.UtilsLogger;

public class TokenConnection {
	
	private TokenConnectionBean bean;
	private HttpURLConnection  urlConnection;
	private UtilsLogger logger;
	
	public TokenConnection(TokenConnectionBean bean, UtilsLogger logger ) {
		this.bean = bean;
		this.logger = logger;
	}
	
	public String getToken() {
		
		String token = "";
		
		try {
			
			System.setProperty("https.protocols", "SSLv3,TLSv1,TLSv1.1,TLSv1.2");
			
			String urlParameters = "grant_type="+bean.getGrantType()+"&client_id="+bean.getClientId()+"&client_secret="+bean.getClientSecret();

			this.logger.write(1, "URL Get Token: " + bean.getUrl());
			this.logger.write(1, "Params: " + urlParameters);
			
			byte[] postData = urlParameters.getBytes();
			
			this.urlConnection = ((HttpURLConnection ) new URL(bean.getUrl().toString()).openConnection());

			this.urlConnection.setConnectTimeout(bean.getConnectTimeout());
			this.urlConnection.setReadTimeout(bean.getReadTimeout());
			this.urlConnection.setDoOutput(true);
			this.urlConnection.setRequestMethod("POST");
			this.urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			this.urlConnection.setRequestProperty("Content-Length", Integer.toString(postData.length));
			this.urlConnection.setUseCaches(false);
			this.urlConnection.setInstanceFollowRedirects(false);
			
			BufferedReader buReader = null;
			OutputStream os = null;

			os = this.urlConnection.getOutputStream();
			os.write(postData);
			os.close();
			
			if (this.urlConnection.getResponseCode() != 200) {
				throw new RuntimeException("Error : HTTP Code" + this.urlConnection.getResponseCode());
			}
			buReader = new BufferedReader(new InputStreamReader(this.urlConnection.getInputStream()));

			JSONObject json = new JSONObject(buReader.readLine());
			
			this.logger.write(1, "Response: " + json.getString("access_token"));
			
			token = json.getString("access_token");

		} catch (Exception e) {
			throw new RuntimeException("Error Get Token : " + e);
		}
		
		return token;

	}

}
