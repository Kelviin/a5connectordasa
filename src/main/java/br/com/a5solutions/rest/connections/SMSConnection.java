package br.com.a5solutions.rest.connections;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import br.com.a5solutions.beans.SMSConnectionBean;
import br.com.a5solutions.utils.UtilsLogger;

public class SMSConnection {
	
	private SMSConnectionBean bean;
	private HttpURLConnection  urlConnection;
	private UtilsLogger logger;
	
	public SMSConnection(SMSConnectionBean bean, UtilsLogger logger) {
		this.bean = bean;
		this.logger = logger;
	}
	
	public boolean sendSMS()  {
		
		boolean sucesso = false;
		
		try {
	
			String bearer = "Bearer " + bean.getToken();
			
			this.logger.write(1, "URL Send SMS: " + bean.getUrl() );
			this.logger.write(1, "Json Body: " + bean.getJsonBody());
			
			this.urlConnection = ((HttpURLConnection ) new URL(bean.getUrl()).openConnection());

			this.urlConnection.setConnectTimeout(bean.getConnectTimeout());
			this.urlConnection.setReadTimeout(bean.getReadTimeout());
			this.urlConnection.setDoOutput(true);
			this.urlConnection.setRequestMethod("POST");
			this.urlConnection.setRequestProperty("Content-Type", "application/json");
			this.urlConnection.setUseCaches(false);
			this.urlConnection.setInstanceFollowRedirects(false);
			this.urlConnection.setRequestProperty ("Authorization", bearer);
			
			BufferedReader buReader = null;
			OutputStream os = null;

			os = this.urlConnection.getOutputStream();
			os.write(bean.getJsonBody().getBytes());
			os.close();
			
			if (this.urlConnection.getResponseCode() != 201) {
				throw new RuntimeException("Error : HTTP Code" + this.urlConnection.getResponseCode());
			}
			buReader = new BufferedReader(new InputStreamReader(this.urlConnection.getInputStream()));

			JSONObject json = new JSONObject(buReader.readLine());
			
			this.logger.write(1, "Response: " +json);
			
			sucesso = true;

		} catch (Exception e) {
			throw new RuntimeException("Falha ao enviar SMS : " + e);
		}
		
		return sucesso;

	}
	
}
