package br.com.a5solutions.rest.connections;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;

import org.json.JSONObject;

import br.com.a5solutions.beans.HorarioAtendimentoConnectionBean;
import br.com.a5solutions.utils.UtilsLogger;

public class HorarioAtendimentoConnection {
	
	private HorarioAtendimentoConnectionBean bean;
	private HttpURLConnection  urlConnection;
	private UtilsLogger logger;
	
	public HorarioAtendimentoConnection(HorarioAtendimentoConnectionBean bean, UtilsLogger logger) {
		this.bean = bean;
		this.logger = logger;
	}
	
	public boolean getHorario()  {
		
		boolean sucesso = false;
		
		try {
			
			this.logger.write(1, "URL Atendimento Horatio: " + bean.getUrl());
			this.logger.write(1, "Json Body: " + bean.getJsonBody());
			
			this.urlConnection = ((HttpURLConnection ) new URL(bean.getUrl()).openConnection());

			this.urlConnection.setConnectTimeout(bean.getConnectTimeout());
			this.urlConnection.setReadTimeout(bean.getReadTimeout());
			this.urlConnection.setDoOutput(true);
			this.urlConnection.setRequestMethod("POST");
			this.urlConnection.setRequestProperty("Content-Type", "application/json");
			this.urlConnection.setUseCaches(false);
			this.urlConnection.setRequestProperty("api-key", UUID.randomUUID().toString());
			
			BufferedReader buReader = null;
			OutputStream os = null;

			os = this.urlConnection.getOutputStream();
			os.write(bean.getJsonBody().getBytes());
			os.close();
			
			if (this.urlConnection.getResponseCode() != 200) {
				throw new RuntimeException("Error : HTTP Code" + this.urlConnection.getResponseCode());
			}
			buReader = new BufferedReader(new InputStreamReader(this.urlConnection.getInputStream()));
			
			JSONObject json = new JSONObject(buReader.readLine());
			
			this.logger.write(1, "Response: " +json);
			
			System.out.println(json.getJSONObject("data").getBoolean("open"));
			sucesso = json.getJSONObject("data").getBoolean("open");

		} catch (Exception e) {
			this.logger.write(1, e.getMessage());
			throw new RuntimeException("Error Get Horario Atendimento : " + e);
		}
		
		return sucesso;

	}
	
}
