package br.com.a5solutions.services;

import java.util.UUID;

import org.json.JSONObject;

import br.com.a5solutions.beans.SMSConnectionBean;
import br.com.a5solutions.beans.TokenConnectionBean;
import br.com.a5solutions.models.SMSModel;
import br.com.a5solutions.rest.connections.SMSConnection;
import br.com.a5solutions.rest.connections.TokenConnection;
import br.com.a5solutions.utils.Propriedades;
import br.com.a5solutions.utils.Propriedades.EnumPropriedade;
import br.com.a5solutions.utils.UtilsLogger;

public class SMSService {
	
	private Propriedades prop = new Propriedades(EnumPropriedade.SMS);
	private UtilsLogger logger;
	
	public SMSService(UtilsLogger logger) {
		this.logger = logger;
	}
	
	public boolean sendSMS(SMSModel sms) {
		
		boolean sucess = false;
		
		try {
			
			logger.write(1, "Service - Send SMS");
			
			SMSConnectionBean bean = new SMSConnectionBean();
			
			bean.setConnectTimeout(Integer.parseInt(prop.getValor("CONNECTION_TIMEOUT")));
			bean.setReadTimeout(Integer.parseInt(prop.getValor("READ_TIMEOUT")));
			bean.setUrl(prop.getValor("SMS_URL"));
			bean.setToken(getToken());
			bean.setJsonBody(generateJsonBody(sms));
			
			SMSConnection smsConnection = new SMSConnection(bean, this.logger);
			sucess = smsConnection.sendSMS();
			
			logger.write(1, "Send SMS sucess: " + sucess);
						
		} catch (Exception e) {
			logger.write(4, "Error Send SMS " + e.getMessage());
			throw new RuntimeException("Error Send SMS " + e);
		}
		
		return sucess;
	}
	
	private String generateJsonBody(SMSModel sms) {
		
		JSONObject json = new JSONObject();
		
		try {
			json.put("ContactKey", "vitorluisdes24041991__");
			json.put("EventDefinitionKey", "APIEvent-6e1897df-591e-f701-74e7-f1c6c17d4515");
			
			JSONObject dataJson = new JSONObject();
			dataJson.put("id", UUID.randomUUID().toString());
			dataJson.put("template", sms.getTemplate());
			dataJson.put("celular", sms.getCellPhone());
			dataJson.put("sender", sms.getSender());
			dataJson.put("local", "BR");
			dataJson.put("marca", sms.getBrand());
			
			json.put("Data", dataJson);
		} catch (Exception e) {
			logger.write(4, "Error Generate json body SMS " + e.getMessage());
			throw new RuntimeException("Error Generate json body SMS " + e);
		}
		
	
		
		return json.toString();
		
	}
	
	private String getToken() {
		String token = ""; 
		
		TokenConnectionBean bean = null;
		
		try {
			
			String urlToken = prop.getValor("SMS_TOKEN_URL");
			String gratType = prop.getValor("SMS_TOKEN_GRANT_TYPE");
			String clientId = prop.getValor("SMS_TOKEN_CLIENT_ID");
			String clientSecret = prop.getValor("SMS_TOKEN_CLIENT_SECRET");
			
			bean = new TokenConnectionBean(urlToken,gratType, clientId, clientSecret);
			bean.setConnectTimeout(Integer.parseInt(prop.getValor("CONNECTION_TIMEOUT")));
			bean.setReadTimeout(Integer.parseInt(prop.getValor("READ_TIMEOUT")));
			
			TokenConnection connection = new TokenConnection(bean, this.logger);
			token = connection.getToken();
			
			logger.write(1, "Token SMS: " + token);
			
		} catch (Exception e) {			
			logger.write(4, "Error Get Token SMS " + e.getMessage());
			throw new RuntimeException("Error GET Token SMS " + e);
		}
		return token;
	}

}
 