package br.com.a5solutions.services;

import org.joda.time.LocalDateTime;
import org.json.JSONObject;

import br.com.a5solutions.beans.HorarioAtendimentoConnectionBean;
import br.com.a5solutions.beans.SMSConnectionBean;
import br.com.a5solutions.rest.connections.HorarioAtendimentoConnection;
import br.com.a5solutions.rest.connections.SMSConnection;
import br.com.a5solutions.utils.Propriedades;
import br.com.a5solutions.utils.Propriedades.EnumPropriedade;
import br.com.a5solutions.utils.UtilsLogger;

public class HorarioAtendimentoService {
	
	private Propriedades prop = new Propriedades(EnumPropriedade.HORARIO);
	private UtilsLogger logger;
	
	public HorarioAtendimentoService(UtilsLogger logger) {
		this.logger = logger;
	}
	
	public boolean getOpen(String identifier) {
		
		boolean sucess = false;
		
		try {
			
			logger.write(1, "Service - Get HorarioAtendimento");
			
			HorarioAtendimentoConnectionBean bean = new HorarioAtendimentoConnectionBean();
			
			bean.setConnectTimeout(Integer.parseInt(prop.getValor("CONNECTION_TIMEOUT")));
			bean.setReadTimeout(Integer.parseInt(prop.getValor("READ_TIMEOUT")));
			bean.setUrl(prop.getValor("URL"));
			bean.setJsonBody(generateJsonBody(identifier));
			
			HorarioAtendimentoConnection connection = new HorarioAtendimentoConnection(bean, this.logger);
			sucess = connection.getHorario();
			
			logger.write(1, "Is it Open: " + sucess);
						
		} catch (Exception e) {
			logger.write(4, "Error Get HorarioAtendimento " + e.getMessage());
			throw new RuntimeException("Error Get HorarioAtendimento " + e);
		}
		
		return sucess;
	}
	
	private String generateJsonBody(String  identifier) {
		
		JSONObject json = new JSONObject();
		
		try {
			json.put("dateTime", new LocalDateTime());
			json.put("partnerIdentifier", identifier);
			
		} catch (Exception e) {
			logger.write(4, "Error Generate json body horario atendimento " + e.getMessage());
			throw new RuntimeException("Error Generate json body horario atendimento " + e);
		}
		
	
		
		return json.toString();
		
	}
}
 