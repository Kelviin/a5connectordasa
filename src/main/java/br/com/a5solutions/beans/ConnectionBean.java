package br.com.a5solutions.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConnectionBean {

	private String url;
	private int connectTimeout;
	private int readTimeout;
	private String user;
	private String password;
	
}
