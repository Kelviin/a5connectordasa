package br.com.a5solutions.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SMSConnectionBean extends ConnectionBean{

	private String token;
	private String jsonBody;
	
}
