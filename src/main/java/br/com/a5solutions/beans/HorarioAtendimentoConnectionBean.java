package br.com.a5solutions.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HorarioAtendimentoConnectionBean extends ConnectionBean{

	private String jsonBody;
	
}
