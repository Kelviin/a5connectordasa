package br.com.a5solutions.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TokenConnectionBean extends ConnectionBean{

	private String url;
	private String grantType;
	private String clientId;
	private String clientSecret;
	
}
