package br.com.a5solutions.controllers;

import com.avaya.sce.runtimecommon.ITraceInfo;

import br.com.a5solutions.models.SMSModel;
import br.com.a5solutions.services.SMSService;
import br.com.a5solutions.utils.UtilsLogger;

public class SMSController {
	
	private UtilsLogger logger;
	private SMSService service;
	
	public SMSController() {
		this.logger= new UtilsLogger();
		this.service = new SMSService(this.logger);
	}
	
	public SMSController(ITraceInfo tr) {
		this.logger= new UtilsLogger(tr);
		this.service = new SMSService(this.logger);
		
	}
	
	public boolean sendSMS(SMSModel sms) {
		return service.sendSMS(sms);
	}

}
