package br.com.a5solutions.controllers;

import com.avaya.sce.runtimecommon.ITraceInfo;

import br.com.a5solutions.services.HorarioAtendimentoService;
import br.com.a5solutions.utils.UtilsLogger;

public class HorarioAtendimentoController {
	
	private UtilsLogger logger;
	private HorarioAtendimentoService service;
	
	public HorarioAtendimentoController() {
		this.logger= new UtilsLogger();
		this.service = new HorarioAtendimentoService(this.logger);
	}
	
	public HorarioAtendimentoController(ITraceInfo tr) {
		this.logger= new UtilsLogger(tr);
		this.service = new HorarioAtendimentoService(this.logger);
		
	}
	
	public boolean getHorario(String identifier) {
		return service.getOpen(identifier);
	}

}
