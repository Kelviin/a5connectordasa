package br.com.a5solutions.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter 
@NoArgsConstructor @AllArgsConstructor
public class SMSModel {
	
	private String team;
	private String cpf;
	private String cip;
	private String birthDate;
	private String brand;
	private String sender;
	private String template;
	private String message;
	private String cellPhone;
}
