package br.com.a5solutions.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Propriedades {

	public enum EnumPropriedade {
		SMS,CONFIG, HORARIO;
	}

	private static final String CONFIG = "ura.properties";
	private static final String SMS = "sms/sms.properties";
	private static final String HORARIO = "atendimento/atendimento.properties";

	private Properties props;
	private String arquivo;

	/**
	 * 
	 * @param tipo - tipo do arquivo de configuracao
	 * @see EnumPropriedade
	 */

	public Propriedades(EnumPropriedade tipo) {
		FileInputStream fis = null;
		String path ="";
		
		if (System.getProperty("os.name").startsWith("Windows")) {
			path = System.getProperty("user.dir")+"/src/resources/";
		} else {
			path = System.getProperty("catalina.home") + "/conf/";
		}

		switch (tipo) {
		case CONFIG:
			arquivo = CONFIG;
			try {
				fis = new FileInputStream(path + arquivo);
				props = new Properties();
				props.load(fis);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (fis != null) {
						fis.close();
					}
				} catch (IOException e) {
					e.getMessage();
				}
			}
			break;
		case SMS:
			arquivo = SMS;
			try {
				fis = new FileInputStream(path + arquivo);
				props = new Properties();
				props.load(fis);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (fis != null) {
						fis.close();
					}
				} catch (IOException e) {
					e.getMessage();
				}
			}
			break;
		case HORARIO:
			arquivo = HORARIO;
			try {
				fis = new FileInputStream(path + arquivo);
				props = new Properties();
				props.load(fis);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (fis != null) {
						fis.close();
					}
				} catch (IOException e) {
					e.getMessage();
				}
			}
			break;

		default:
			System.out.println("Tipo de properties invalida");
			break;
		}
	}

	public String getArquivo() {
		return arquivo;
	}

	/**
	 * 
	 * @param String propriedade
	 * @return retorna o valor da propriedade
	 */
	public String getValor(String propriedade) {
		String valor = props.getProperty(propriedade);
		return valor;
	}

}
