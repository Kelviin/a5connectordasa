package br.com.a5solutions.utils;

import com.avaya.sce.runtimecommon.ITraceInfo;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor @AllArgsConstructor
public class UtilsLogger {

	 private ITraceInfo tr;
	
	  public void write(int level, String msg)
	  {
		  
		  try {
			  if (this.tr != null) {
			      this.tr.writeln(level, "#####" + msg);
			    } else {
			      System.out.println(msg);
			    }
		} catch (Exception e) {
			e.getStackTrace();
		}
		  
	  
	  }
	
}
